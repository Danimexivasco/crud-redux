import {
    AGREGAR_PRODUCTO,
    AGREGAR_PRODUCTO_EXITO,
    AGREGAR_PRODUCTO_ERROR,
    COMENZAR_DESCARGA_PRODUCTOS,
    DESCARGA_PRODUCTOS_EXITO,
    DESCARGA_PRODUCTOS_ERROR,
    OBTENER_PRODUCTO_ELIMINAR,
    PRODUCTO_ELIMINADO_EXITO,
    PRODUCTO_ELIMINADO_ERROR,
    OBTENER_PRODUCTO_EDITAR,
    COMENZAR_EDICION_PRODUCTO,
    PRODUCTO_EDITADO_EXITO,
    PRODUCTO_EDITADO_ERROR
} from '../types';

import Swal from 'sweetalert2';

import clienteAxios from '../config/axios';

// CREAR nuevos productos
export function crearNuevoProductoAction(producto) {
    return async (dispatch) => {
        dispatch(agregarProducto())
        try {
            // Insertar en la API
            await clienteAxios.post('/productos', producto);
            // Si todo sale bien
            dispatch(agregarProductoExito(producto));
            Swal.fire(
                'Correcto',
                'El producto se agregó correctamente',
                'success'
            )
        } catch (error) {
            console.log(error)
            // Si hay un error
            dispatch(agregarProductoError(true));
            Swal.fire({
                icon: 'error',
                title: 'Hubo un error',
                text: 'Hubo un error, intenta de nuevo'
            })
        }
    }
}


// agregar producto
const agregarProducto = () => {
    return {
        type: AGREGAR_PRODUCTO,
    }
}

// Si el producto se guarda en la bbdd
const agregarProductoExito = (producto) => {
    return {
        type: AGREGAR_PRODUCTO_EXITO,
        payload: producto
    }
}

// si hay error al guardar el producto
const agregarProductoError = (estado) => {
    return {
        type: AGREGAR_PRODUCTO_ERROR,
        payload: estado
    }
}


// OBTENER LOS PRODUCTOS
export function obtenerProductosAction() {
    return async (dispatch) => {
        dispatch(descargarProductos());
        try {

            const respuesta = await clienteAxios.get('/productos')
            // Si todo sale bien
            dispatch(descargarProductosExito(respuesta.data))
        } catch (error) {
            console.log(error)
            dispatch(descargarProductosError(true))
            Swal.fire({
                icon: 'error',
                title: 'Ooops algo falló',
                text: 'Intenta recargar la página'
            })
        }
    }
}

// Funcion que descarga los productos de la bbdd
const descargarProductos = () => ({
    type: COMENZAR_DESCARGA_PRODUCTOS,
    payload: true
})

// Si no hay error trayendoselos
const descargarProductosExito = (productos) => ({
    type: DESCARGA_PRODUCTOS_EXITO,
    payload: productos
})

// Si hay error trayendoselos
const descargarProductosError = (estado) => ({
    type: DESCARGA_PRODUCTOS_ERROR,
    payload: estado
})


// ELIMINAR productos
export function borrarProductoAction(id) {
    return async (dispatch) => {
        dispatch(obtenerProductoEliminar(id));
        try {
            // eliminar producto de la API
            await clienteAxios.delete(`/productos/${id}`);
            // Si tenemos exito eliminando
            dispatch(eliminarProductoExito());
            // Si se elimina mostrar alerta 
            Swal.fire(
                'Eliminado!',
                'El producto se eliminó correctamente.',
                'success'
            )
        } catch (error) {
            console.log(error)
            // si hay algun error eliminando 
            dispatch(eliminarProductoError());
        }
    }
}

// Obtener el producto que se va a eliminar
const obtenerProductoEliminar = (id) => ({
    type: OBTENER_PRODUCTO_ELIMINAR,
    payload: id
})

// Si tenemos exito
const eliminarProductoExito = () => ({
    type: PRODUCTO_ELIMINADO_EXITO
})

// Si hay algun error
const eliminarProductoError = () => ({
    type: PRODUCTO_ELIMINADO_ERROR,
    payload: true
})


// EDITAR productos
export function obtenerProductoEditarAction(producto) {
    return async (dispatch) => {
        dispatch(obtenerProductoEditar(producto));
    }
}

// Obtener producto a editar
const obtenerProductoEditar = (producto) => ({
    type: OBTENER_PRODUCTO_EDITAR,
    payload: producto
})

// Edita un registro en la API y State
export function editarProductoAction (producto){
    return async (dispatch) => {
        dispatch(editarProducto());
        try {   
            await clienteAxios.put(`/productos/${producto.id}`, producto)
            dispatch(editarProductoExito(producto))
        } catch (error) {
            console.log(error);
            dispatch(editarProductoError())
        }
    }
}

const editarProducto = () => ({
    type: COMENZAR_EDICION_PRODUCTO
})


// Si tenemos exito editando
const editarProductoExito = (producto) => ({
    type: PRODUCTO_EDITADO_EXITO,
    payload: producto
})

// Si hay algun error
const editarProductoError = () => ({
    type: PRODUCTO_EDITADO_ERROR,
    payload: true
})