import React, {useState} from 'react';
import { useDispatch, useSelector } from 'react-redux';

// Actions de Redux
import { crearNuevoProductoAction } from '../actions/productoActions';
import {mostrarAlertaAction, ocultarAlertaAction} from '../actions/alertaActions';


const NuevoProducto = ({history}) => {

    // State del componente 
    const [nombre, setNombre] = useState('');
    const [precio, setPrecio] = useState(0);
    // const [errorForm, setErrorForm] = useState(false);

    // Utilizar useDispatch y devuelve una fución
    const dispatch = useDispatch();

    // Acceder al state del store
    const cargando = useSelector( (state) => state.productos.loading);
    const error = useSelector( (state) => state.productos.error);

    const alerta = useSelector((state) => state.alerta.alerta)

    // Mandar llamar el action de productoAction
    const agregarProducto = (producto) => dispatch( crearNuevoProductoAction(producto) )


    // Cuando el usuario haga submit de nuevo producto
    const submitNuevoProducto = (e) => {
        e.preventDefault();
        

        // Validar
        if(nombre.trim() === '' || precio <= 0){
            // setErrorForm(true)
            const alerta = {
                msg: 'Todos los campos son obligatorios',
                clases: 'alert alert-danger mt-4 text-center'
            }
            dispatch(mostrarAlertaAction(alerta));
            return;
        }

        // Si no hay errores
        // setErrorForm(false)
        dispatch(ocultarAlertaAction())

        // Crear el nuevo producto
        agregarProducto({
            nombre,
            precio
        });
        
        // setTimeout(()=>{
            history.push('/');

        // },1000)
        

    }

    return (
        <div className="row justify-content-center">
            <div className="col-md-8">
                <div className="card">
                    <div className="card-body">
                        <h2 className="text-center mb-4 font-weight-bold">
                            Agregar Producto
                        </h2>

                        <form
                            onSubmit={submitNuevoProducto}
                        >
                            
                            <div className="form-group">
                                <label>Nombre Producto</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Nombre producto"
                                    name="nombre"
                                    value={nombre}
                                    onChange={e => setNombre(e.target.value)}
                                />
                            </div>
                            <div className="form-group">
                                <label>Precio Producto</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Precio producto (€)"
                                    name="precio"
                                    value={precio}
                                    onChange = {e => setPrecio(e.target.value)}
                                    // onChange = {e => setPrecio(Number(e.target.value))}
                                />
                            </div>
                            {alerta ? <p className={alerta.clases}>{alerta.msg}</p> : null}
                            <button
                                type="submit"
                                className="btn btn-primary font-weight-bold text-uppercase d-block w-100"

                            >Agregar</button>
                        </form>
                        {/* {errorForm ? <p className="alert alert-danger mt-4 text-center">Todos los campos son obligatorios</p> : null} */}
                        
                        {error ? <p className="alert alert-danger p-2 mt-4 text-center"> Hubo un error  </p> : null}
                        {cargando ? <p>Cargando...</p> : null}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default NuevoProducto;