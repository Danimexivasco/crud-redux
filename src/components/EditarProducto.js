import React, {useState, useEffect} from 'react';
import {useHistory, Redirect} from 'react-router-dom';

// Redux
import {useDispatch, useSelector} from 'react-redux';
import {editarProductoAction} from '../actions/productoActions';

const EditarProducto = () => {

    
    const history = useHistory();
    const dispatch = useDispatch();

    // Nuevo state del producto
    const [productoForm, setProductoForm] = useState({
        nombre: '',
        precio: 0
    })


    // Producto a editar
    const producto = useSelector((state) => state.productos.productoEditar);

    // Llenar el state automaticamente
    useEffect(()=>{
        setProductoForm(producto)
    },[producto]);

    if(!producto){
       return <Redirect to ="/" />
    }
    
    // Leer los datos del form
    const onChangeForm = (e) => {
        setProductoForm({
            ...productoForm,
            [e.target.name]: e.target.value
        })
    }


    // const {nombre, precio} = producto;

    // Submit para editar producto
    const submitEditarProducto = (e) => {
        e.preventDefault();

        dispatch(editarProductoAction(productoForm))

        setTimeout(() => {
            history.push('/');
        }, 300);
        

    }

    return ( 
        <div className="row justify-content-center">
            <div className="col-md-8">
                <div className="card">
                    <div className="card-body">
                        <h2 className="text-center mb-4 font-weight-bold">
                            Editar Producto
                        </h2>

                        <form
                            onSubmit={submitEditarProducto}
                        >
                            <div className="form-group">
                                <label>Nombre Producto</label>
                                <input 
                                    type="text"
                                    className="form-control"
                                    placeholder="Nombre producto"
                                    name="nombre"
                                    value={productoForm.nombre}
                                    onChange = {onChangeForm}
                                />
                            </div>
                            <div className="form-group">
                                <label>Precio Producto</label>
                                <input 
                                    type="text"
                                    className="form-control"
                                    placeholder="Precio producto (€)"
                                    name="precio"
                                    value={productoForm.precio}
                                    onChange = {onChangeForm}
                                />
                            </div>
                            <button
                                type="submit"
                                className="btn btn-primary font-weight-bold text-uppercase d-block w-100"
                               
                            >Guardar cambios</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
     );
}
 
export default EditarProducto;