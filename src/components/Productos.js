import React, { Fragment, useEffect } from 'react';
import Producto from './Producto';

// Redux
import {useDispatch, useSelector} from 'react-redux';
import {obtenerProductosAction} from '../actions/productoActions';


const Productos = () => {

    const dispatch = useDispatch();

    useEffect(() => {
        // Consultar la API
        const cargarProductos = () => dispatch(obtenerProductosAction());
        setTimeout(() => {
        cargarProductos();
        }, 500);
        //eslint-disable-next-line
    }, []);
    
    // Obtener el STATE
    const productos = useSelector(state => state.productos.productos);
    // console.log(productos)

    return ( 
        <Fragment>
            <h2 className="text-center my-5">Listado de Productos</h2>
            <table className="table table-striped">
                <thead className="bg-primary">
                    <tr>
                        <th scope="col" className="text-center">Nombre</th>
                        <th scope="col" className="text-center">Precio</th>
                        <th scope="col" className="text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody className="text-center">
                    {productos.length === 0 ? <tr><th>No hay productos</th></tr> : (
                        productos.map(producto=>{
                            return(
                                <Producto 
                                    key = {producto.id}
                                    producto = {producto}
                                    // informacion = {producto.informacion}
                                />
                            )
                        })
                    )}
                </tbody>
            </table>
        </Fragment>
     );
}
 
export default Productos;