import React from 'react';
import {useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';

// Redux
import { useDispatch } from 'react-redux';
import { borrarProductoAction, obtenerProductoEditarAction } from '../actions/productoActions';

const Producto = ({ producto }) => {
    const { nombre, precio, id } = producto;
    // console.log(informacion)

    const dispatch = useDispatch();
    const history = useHistory(); //Habilitar history para redireccion

    const confirmarEliminarProducto = (id) => {

        // Confirmar eliminación
        Swal.fire({
            title: '¿Estas seguro?',
            text: "Esta acción no se podrá revertir",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar!',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                // Pasarlo al action
                dispatch(borrarProductoAction(id))
                
            }
        })

    }

    // Funcion para redireccionar al editar un producto
    const redireccionEditar = (producto) => {
        dispatch(obtenerProductoEditarAction(producto))
        history.push(`/productos/editar/${producto.id}`);
    }

    return (
        <tr >
            <td>{nombre}</td>
            <td className="font-weight-bold">{precio} €</td>
            <td className="acciones">

                <button
                    type= "button"
                    className="btn btn-primary mr-5"
                    onClick={() => redireccionEditar(producto)}
                >Editar</button>
                {/* <Link to={`/productos/editar/${id}`} className="btn btn-primary mr-5">
                    Editar
                </Link> */}
                <button
                    type="button"
                    className="btn btn-danger"
                    id="btnBorrar"
                    onClick={() => confirmarEliminarProducto(id)}
                >Eliminar</button>
            </td>
        </tr>

    );
}

export default Producto;